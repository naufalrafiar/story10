"""myweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from register import views as v
from welcomepage import views as w
from logoutpage import views as l

urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/', v.register, name="index"),
    path('', include("django.contrib.auth.urls")),
    path('', w.my_view, name="User"),
    path('thankyou/', l.logoutpage )
]
