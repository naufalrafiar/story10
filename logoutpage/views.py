from django.shortcuts import render

# Create your views here.
def logoutpage(request):
    return render(request, "logout.html")